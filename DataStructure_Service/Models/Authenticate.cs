﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.IO;
using System.Data.SqlClient;
using System.Data;
using System.Collections.Specialized;

namespace DataStructure_Service.Models
{
    public class Authenticate
    {
        //SqlConnection con = new SqlConnection(@"Data Source=ec2-35-154-179-138.ap-south-1.compute.amazonaws.com,1702;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=Live;Password=H3@!th$c!0n7!^3");

        SqlConnection con = new SqlConnection(Authenticate.ProdliveConnectionString());
        internal string authenticateUser(string userId)
        {
             
            string userPasscode = GenerateOTPNew(); //Authenticate_short.Encrypt(passcode); //Encrypt(passcode, userId);
            if (userPasscode != null)
            {
                string response = saveauthenticationKey(userId, userPasscode);
                if (response == "success")
                {
                    string messageBody = "Your authentication key is: " + userPasscode;

                    SendSmslogonutilityservice("9818118603", messageBody);
                    return "You are authenticated successfully.";
                }
                else
                {
                    return "Not authenticated";
                }
            }
            else
            {
                return "Not authenticated";
            }
        }
   
        public string GenerateOTPNew()
        {
            char[] charArr = "0123456789".ToCharArray();
            string strrandom = string.Empty;
            Random objran = new Random();
            int noofcharacters = 6;
            for (int i = 0; i < noofcharacters; i++)
            {
                int pos = objran.Next(1, charArr.Length);
                if (!strrandom.Contains(charArr.GetValue(pos).ToString()))
                    strrandom += charArr.GetValue(pos);
                else
                    i--;
            }
            return strrandom;
        }
        internal string saveauthenticationKey(string userId, string passcode)
        {
            try
            {
                
                SqlCommand cmd = new SqlCommand();
                cmd.CommandText = "select UserId,PassCode,ExpiryTime from HMs_Datastructure_authentication where UserId=@userid";
                cmd.Connection = con;
                cmd.Parameters.AddWithValue("@userid", userId);
                SqlDataAdapter ad = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                ad.Fill(ds);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    SqlCommand cmdUpdate = new SqlCommand();
                    cmdUpdate.CommandText = "update HMs_Datastructure_authentication set PassCode=@PassCode,ExpiryTime=@ExpiryTime where UserId=@userid";
                    cmdUpdate.Connection = con;
                    cmdUpdate.Parameters.AddWithValue("@userid", userId);
                    cmdUpdate.Parameters.AddWithValue("@PassCode", passcode);
                    cmdUpdate.Parameters.AddWithValue("@ExpiryTime", DateTime.Now);
                    SqlDataAdapter adUpdate = new SqlDataAdapter(cmdUpdate);
                    DataSet dsUpdate = new DataSet();
                    adUpdate.Fill(dsUpdate);
                }
                else
                {
                    return "Not authenticated.";
                    //SqlCommand cmdInsert = new SqlCommand();
                    //cmdInsert.CommandText = "insert into HMs_Datastructure_authentication values (@userid,@PassCode,@ExpiryTime)";
                    //cmdInsert.Connection = con;
                    //cmdInsert.Parameters.AddWithValue("@userid", userId);
                    //cmdInsert.Parameters.AddWithValue("@PassCode", passcode);
                    //cmdInsert.Parameters.AddWithValue("@ExpiryTime", DateTime.Now);
                    //SqlDataAdapter adInsert = new SqlDataAdapter(cmdInsert);
                    //DataSet dsInsert = new DataSet();
                    //adInsert.Fill(dsInsert);
                }
                return "success";
            }
            catch (Exception ex)
            {
                return "error";
            }
        }
        internal void SendSmslogonutilityservice(string contactNo, string messageBody)
        {
            string responseString = string.Empty;
            string authKey = "52ea4bf2dd576";
            string mobileNumber = contactNo;
            string senderId = "SCIONS";
            string message = messageBody;
            StringBuilder sbPostData = new StringBuilder();
            sbPostData.AppendFormat("key={0}", authKey);
            sbPostData.AppendFormat("&campaign={0}", "64");
            sbPostData.AppendFormat("&routeid={0}", "20");
            sbPostData.AppendFormat("&type={0}", "text");
            sbPostData.AppendFormat("&contacts={0}", mobileNumber);
            sbPostData.AppendFormat("&senderid={0}", senderId);
            sbPostData.AppendFormat("&msg={0}", message);
            sbPostData.AppendFormat("&time={0}", "");
            string sendSMSUri = "http://logonutility.in/app/smsapi/index.php";
            HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(sendSMSUri);
            UTF8Encoding encoding = new UTF8Encoding();
            byte[] data = encoding.GetBytes(sbPostData.ToString());
            httpWReq.Method = "POST";
            httpWReq.ContentType = "application/x-www-form-urlencoded";
            httpWReq.ContentLength = data.Length;
            using (Stream stream = httpWReq.GetRequestStream())
            {
                stream.Write(data, 0, data.Length);
            }
            WebClient client = new WebClient(); string abc = string.Empty;
            abc = client.DownloadString(sendSMSUri);
            HttpWebResponse response = (HttpWebResponse)httpWReq.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream());
            responseString = reader.ReadToEnd();
            reader.Close();
            response.Close();

        }
        internal bool checkUserAuthentication(string passcode, string userId)
        {
            //SqlConnection con = new SqlConnection(@"Data Source=ec2-35-154-179-138.ap-south-1.compute.amazonaws.com,1702;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=Live;Password=H3@!th$c!0n7!^3");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select UserId,PassCode,ExpiryTime from HMs_Datastructure_authentication where UserId=@userid";
            cmd.Parameters.AddWithValue("@userid", userId);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(ds.Tables[0].Rows[0]["PassCode"].ToString()))
                {
                    if (ds.Tables[0].Rows[0]["PassCode"].ToString() == passcode)
                    {
                        DateTime currentTime = DateTime.Now;
                        DateTime expiryTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["ExpiryTime"]);
                        TimeSpan span = currentTime.Subtract(expiryTime);
                        double difference = span.TotalMinutes;
                        if (difference > 15)
                        {
                            return false;
                        }
                        else
                        {
                            return true;
                        }

                    }
                    else {
                        return false;
                    }
                }
                return false;
            }
            return false;
        }
        internal bool checkRunningProcess(string userId)
        {           
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select UserId,RunningProcess from HMs_Datastructure_authentication where UserId=@userid";
            cmd.Parameters.AddWithValue("@userid", userId);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["RunningProcess"]))
                {
                    return true;
                }
                return false;
            }
            return false;
        }
        internal void setRunningProcess(string userId)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "select UserId,RunningProcess from HMs_Datastructure_authentication where UserId=@userid";
            cmd.Parameters.AddWithValue("@userid", userId);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                string query = "";
                if (Convert.ToBoolean(ds.Tables[0].Rows[0]["RunningProcess"]))
                {
                    query = "update HMs_Datastructure_authentication set RunningProcess=0 where UserId=@userid";
                }
                else {
                    query = "update HMs_Datastructure_authentication set RunningProcess=1 where UserId=@userid";
                }
                SqlCommand cmdUpdate = new SqlCommand();
                cmdUpdate.CommandText = query;
                cmdUpdate.Parameters.AddWithValue("@userid", userId);
                cmdUpdate.Connection = con;
                SqlDataAdapter adUpdate = new SqlDataAdapter(cmdUpdate);
                DataSet dsUpdate = new DataSet();
                adUpdate.Fill(dsUpdate);
            }             
        }
        internal DataSet GetBillingInformation(DateTime from, DateTime to)
        {
           // SqlConnection con = new SqlConnection(@"Data Source=ec2-35-154-179-138.ap-south-1.compute.amazonaws.com,1702;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=Live;Password=H3@!th$c!0n7!^3");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DataStructure_BillingInformation";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromDate", from);
            cmd.Parameters.AddWithValue("@toDate", to);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }
        internal DataSet GetPatientRegistrationInformation(DateTime from, DateTime to)
        {
            // SqlConnection con = new SqlConnection(@"Data Source=ec2-35-154-179-138.ap-south-1.compute.amazonaws.com,1702;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=Live;Password=H3@!th$c!0n7!^3");
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DataStructure_PatientRegistration";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromDate", from);
            cmd.Parameters.AddWithValue("@toDate", to);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }
        public static string ProdliveConnectionString()
        {
            NameValueCollection appConfig = System.Configuration.ConfigurationManager.AppSettings;
            string param1 = appConfig["PARAM1"].ToLower();
            if (param1.ToLower() == "live")
            {
                return @"Data Source=ec2-35-154-179-138.ap-south-1.compute.amazonaws.com,1702;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=Live;Password=H3@!th$c!0n7!^3;Max Pool Size=200;Connect Timeout=0;";
            }
            else {
                return @"Data Source=CC-SWDEPLOY\CCSQLDEV;Initial Catalog=CC_DB_PRODLIVE;Persist Security Info=True;User ID=sa;Password=C!0udCh0wk$%^;Max Pool Size=200;Connect Timeout=0;";
            }
            
        }
        internal DataSet GetApplications(DateTime from, DateTime to)
        {            
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DataStructure_Applications";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromDate", from);
            cmd.Parameters.AddWithValue("@toDate", to);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }
        internal DataSet GetTestResults(Guid ApplicationId,DateTime from,DateTime to)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DataStructure_TestResults";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromDate", from);
            cmd.Parameters.AddWithValue("@toDate", to);
            cmd.Parameters.AddWithValue("@applicationId", ApplicationId);
       
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }
        internal DataSet GetAntibioticResults(Guid ApplicationId, DateTime from, DateTime to)
        {
            SqlCommand cmd = new SqlCommand();
            cmd.CommandText = "DataStructure_AntibioticTable";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@fromDate", from);
            cmd.Parameters.AddWithValue("@toDate", to);
            cmd.Parameters.AddWithValue("@applicationId", ApplicationId);
            cmd.Connection = con;
            SqlDataAdapter ad = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            ad.Fill(ds);
            return ds;
        }        
    }
    public class RootObject
    {
        public string UserName { get; set; }
        public DateTime date { get; set; }
        public string passcode { get; set; }
         
    }
    
}