﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Newtonsoft.Json.Linq;
using DataStructure_Service.Models;
using System.Web.Http.Results;
using System.Data;

namespace DataStructure_Service.Controllers
{
    public class ValuesController : ApiController
    {   
        [HttpPost]         
        public string AuthenticateMe([FromBody]RootObject Data)
        {
            try
            {
                Authenticate ob = new Authenticate();
                string response = ob.authenticateUser(Data.UserName);
                return response;
            }
            catch (Exception ex) {
                return "Error";
            }
        }
        // POST api/values
        [HttpPost]        
        public HttpResponseMessage GetBillingData(RootObject Data)
        {
            try
            {
                Authenticate ob = new Authenticate();
                bool userFlag = ob.checkUserAuthentication(Data.passcode, Data.UserName);
                if (userFlag)
                {
                    if (!ob.checkRunningProcess(Data.UserName))
                    {
                       ob.setRunningProcess(Data.UserName);
                        DateTime to = Data.date;
                        DateTime from = to.AddDays(-6);
                        DataSet ds = ob.GetBillingInformation(from, to);
                        ob.setRunningProcess(Data.UserName);
                        return Request.CreateResponse(HttpStatusCode.OK, ds);
                    }
                    else {
                        return Request.CreateResponse(HttpStatusCode.OK, "Concurrent connections are not allowed.");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "you are not authorised.");
                }
            }
            catch(Exception ex) {

                return Request.CreateResponse(HttpStatusCode.OK, ex.Message);
            }

        }

        [HttpPost]
        public HttpResponseMessage GetPatientRegistrationData(RootObject Data)
        {
            try
            {
                Authenticate ob = new Authenticate();
                bool userFlag = ob.checkUserAuthentication(Data.passcode, Data.UserName);
                if (userFlag)
                {
                    if (!ob.checkRunningProcess(Data.UserName))
                    {
                        ob.setRunningProcess(Data.UserName);
                        DateTime to = Data.date;
                        DateTime from = to.AddDays(-6);
                        DataSet ds = ob.GetPatientRegistrationInformation(from, to);
                        ob.setRunningProcess(Data.UserName);
                        return Request.CreateResponse(HttpStatusCode.OK, ds);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Concurrent connections are not allowed.");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "you are not authorised.");
                }
            }
            catch (Exception ex) {

                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }

        }

        [HttpPost]
        public HttpResponseMessage GetTestDetails(RootObject Data)
        {
            try
            {
                Authenticate ob = new Authenticate();
                bool userFlag = ob.checkUserAuthentication(Data.passcode, Data.UserName);
                DataSet dsResult = new DataSet();
                if (userFlag)
                {
                    if (!ob.checkRunningProcess(Data.UserName))
                    {
                        ob.setRunningProcess(Data.UserName);
                        DateTime to = Data.date;
                        for (int i = 1; i <= 6; i++)
                        {
                            DateTime from = to.AddDays(-1);
                            DataSet dsApplicationId = ob.GetApplications(from, to);
                            if (dsApplicationId.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dsApplicationId.Tables[0].Rows.Count; j++)
                                {
                                    DataSet temp = ob.GetTestResults(Guid.Parse(dsApplicationId.Tables[0].Rows[j]["ApplicationId"].ToString()), from, to);
                                    if (temp.Tables[0].Rows.Count > 0)
                                    {
                                        dsResult.Merge(temp);
                                    }
                                }
                            }
                            to = from;
                        }
                        ob.setRunningProcess(Data.UserName);
                        return Request.CreateResponse(HttpStatusCode.OK, dsResult);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Concurrent connections are not allowed.");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "you are not authorised.");
                }
            }
            catch (Exception ex) {

                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }

        }

        [HttpPost]
        public HttpResponseMessage GetAntibioticDetails(RootObject Data)
        {
            try
            {
                Authenticate ob = new Authenticate();
                bool userFlag = ob.checkUserAuthentication(Data.passcode, Data.UserName);
                DataSet dsResult = new DataSet();
                if (userFlag)
                {
                    if (!ob.checkRunningProcess(Data.UserName))
                    {
                        ob.setRunningProcess(Data.UserName);
                        DateTime to = Data.date;
                        for (int i = 1; i <= 6; i++)
                        {
                            DateTime from = to.AddDays(-1);
                            DataSet dsApplicationId = ob.GetApplications(from, to);
                            if (dsApplicationId.Tables[0].Rows.Count > 0)
                            {
                                for (int j = 0; j < dsApplicationId.Tables[0].Rows.Count; j++)
                                {
                                    DataSet temp = ob.GetAntibioticResults(Guid.Parse(dsApplicationId.Tables[0].Rows[j]["ApplicationId"].ToString()), from, to);
                                    if (temp.Tables[0].Rows.Count > 0)
                                    {
                                        dsResult.Merge(temp);
                                    }
                                }
                            }
                            to = from;
                        }
                        ob.setRunningProcess(Data.UserName);
                        return Request.CreateResponse(HttpStatusCode.OK, dsResult);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, "Concurrent connections are not allowed.");
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "you are not authorised.");
                }
            }
            catch (Exception ex) {
                return Request.CreateResponse(HttpStatusCode.OK, "Error");
            }
        }
    }
}
