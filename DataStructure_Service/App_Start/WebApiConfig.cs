﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace DataStructure_Service
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                 name: "Billing",
                 routeTemplate: "GetAuthenticate/{UserName}",
                 defaults: new { controller = "Values", action = "AuthenticateMe", UserName = RouteParameter.Optional }
             );

            config.Routes.MapHttpRoute(
                name: "BillingData",
                routeTemplate: "BillingData",
                defaults: new { controller = "Values", action = "GetBillingData" }
            );
            config.Routes.MapHttpRoute(
                name: "PatientData",
                routeTemplate: "PatientData",
                defaults: new { controller = "Values", action = "GetPatientRegistrationData" }
            );
            config.Routes.MapHttpRoute(
               name: "TestDetails",
               routeTemplate: "TestData",
               defaults: new { controller = "Values", action = "GetTestDetails" }
           );
            config.Routes.MapHttpRoute(
              name: "AntibioticDetails",
              routeTemplate: "AntibioticData",
              defaults: new { controller = "Values", action = "GetAntibioticDetails" }
          );

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
